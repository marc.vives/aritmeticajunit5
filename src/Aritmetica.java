public class Aritmetica {

    public int suma(int a, int b) {
        return a + b;
    }

    public double suma(double a, double b) {
        return a + b;
    }

    public int resta(int a, int b) {
        int resultat = a - b;
        return resultat;
    }
}
